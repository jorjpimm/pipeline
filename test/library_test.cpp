#include "pipeline/pipeline.h"

#include <catch/catch.hpp>


SCENARIO("node types can be created", "[library]")
{
  GIVEN("A simple node type")
  {
    struct SimpleNode : public pipeline::Node
    {
      void compute() const {}
    };

    pipeline::Allocator alloc{ std::allocator<void>{} };
    auto type = pipeline::NodeType::create<SimpleNode>(alloc, "dummy");

    THEN("The node is named correctly")
    {
      REQUIRE(type.name() == "dummy");
    }

    THEN("The node has no data types")
    {
      REQUIRE(!type.has_runner_data());
      REQUIRE(!type.has_instance_data());
    }
  }

  GIVEN("A node with runner data")
  {
    struct SimpleNode : public pipeline::Node
    {
      struct RunnerData { };
      void compute(RunnerData &) const {}
    };

    pipeline::Allocator alloc{ std::allocator<void>{} };
    auto type = pipeline::NodeType::create<SimpleNode>(alloc, "dummy");


    THEN("The node has correct data types")
    {
      REQUIRE(type.has_runner_data());
      REQUIRE(!type.has_instance_data());
    }
  }

  GIVEN("A node with instance data")
  {
    struct SimpleNode : public pipeline::Node
    {
      struct InstanceData { };
      void compute(InstanceData &) const {}
    };

    pipeline::Allocator alloc{ std::allocator<void>{} };
    auto type = pipeline::NodeType::create<SimpleNode>(alloc, "dummy");


    THEN("The node has correct data types")
    {
      REQUIRE(!type.has_runner_data());
      REQUIRE(type.has_instance_data());
    }
  }
}

SCENARIO("node types can have input", "[library]")
{
  GIVEN("A node with no compute input")
  {
    struct SimpleNode : public pipeline::Node
    {
      void compute() const {}
    };

    pipeline::Allocator alloc{ std::allocator<void>{} };
    auto type = pipeline::NodeType::create<SimpleNode>(alloc, "dummy");

    THEN("The node has correct input data types")
    {
      REQUIRE(type.compute_inputs() == std::vector<pipeline::NodeType::ConnectionDataType>());
    }
  }

  GIVEN("A node with one compute input")
  {
    struct InputType {};
    struct SimpleNode : public pipeline::Node
    {
      void compute(InputType) const {}
    };

    pipeline::Allocator alloc{ std::allocator<void>{} };
    auto type = pipeline::NodeType::create<SimpleNode>(alloc, "dummy");

    THEN("The node has correct input data types")
    {
      REQUIRE(type.compute_inputs() == 
        std::vector<pipeline::NodeType::ConnectionDataType>{ pipeline::NodeType::ConnectionDataType::create<InputType>() });
    }
  }

  GIVEN("A node with multiple qualified compute input")
  {
    struct InputType1 {};
    struct InputType2 {};
    struct SimpleNode : public pipeline::Node
    {
      void compute(const InputType1 &, const InputType2 &) const {}
    };

    pipeline::Allocator alloc{ std::allocator<void>{} };
    auto type = pipeline::NodeType::create<SimpleNode>(alloc, "dummy");

    THEN("The node has correct input data types")
    {
      auto expected = std::vector<pipeline::NodeType::ConnectionDataType>{
        pipeline::NodeType::ConnectionDataType::create<InputType1>(),
        pipeline::NodeType::ConnectionDataType::create<InputType2>()
      };
      REQUIRE(type.compute_inputs() == expected);
    }
  }
}

SCENARIO("library can be populated", "[library]") 
{
  GIVEN("An empty node library") 
  {
    pipeline::Allocator alloc{ std::allocator<void>{} };
    pipeline::NodeLibrary library{ alloc };

    WHEN("The node is registered")
    {
      THEN("The library has no contents")
      {
        REQUIRE(library.node_count() == 0);
      }

      struct DummyNode : public pipeline::Node 
      {
        void compute() const { }
      };
      auto registered = library.register_node<DummyNode>("dummy");

      THEN("The library has contents") 
      {
        REQUIRE(library.node_count() == 1);
        REQUIRE(library.find("dummy") == registered);
      }

    }
  }
}
