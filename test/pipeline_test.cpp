#include "pipeline/pipeline.h"

#include <catch/catch.hpp>

struct Adder : public pipeline::Node
{
  int compute(int a, int b) const { return a + b; }
};

struct Comparator : public pipeline::Node
{
  int _comp_to;
  Comparator(int comp_to)
    : _comp_to(comp_to)
  {
  }

  bool compute(int a) const { return _comp_to == a; }

};

SCENARIO("nodes can be executed", "[node]")
{
  GIVEN("A node")
  {
    pipeline::Allocator alloc{ std::allocator<void>{} };
    auto t = pipeline::NodeType::create<Adder>(alloc, "adder");
    
    THEN("The node can be executed with tuple")
    {
      int a = 5;
      int b = 4;
      const void *args[] = { &a, &b };
      int out = 0;
      void *outputs[] = { &out };

      Adder adder;
      t.compute(adder, nullptr, nullptr, args, outputs);
    }
  }
}


SCENARIO("pipeline description can be populated", "[pipeline]")
{
  GIVEN("A node library")
  {
    pipeline::Allocator alloc{ std::allocator<void>{} };
    pipeline::NodeLibrary library{ alloc };
    
    auto adder = library.register_node<Adder>("Adder");
    auto comparator = library.register_node<Comparator>("Comparator");

    WHEN("The nodes can be added to a description")
    {
      pipeline::PipelineDescription desc(alloc, library);
      THEN("The library has no contents")
      {
        REQUIRE(desc.node_count() == 0);
      }

      auto add_node = desc.add("Adder");
      auto compare_node = desc.add("Comparator");

      THEN("The library has content")
      {
        REQUIRE(desc.node_count() == 2);
      }

      REQUIRE(!desc.can_connect(compare_node, add_node));
      REQUIRE(desc.can_connect(add_node, compare_node));
      desc.connect(add_node, compare_node);
      REQUIRE(!desc.can_connect(add_node, compare_node));

      THEN("Nodes are connected")
      {
        REQUIRE(desc.are_connected(add_node, compare_node));
      }
    }
  }
}

SCENARIO("pipeline can be populated", "[pipeline]")
{
  GIVEN("A pipeline description")
  {
    pipeline::Allocator alloc{ std::allocator<void>{} };
    pipeline::NodeLibrary library{ alloc };

    auto adder = library.register_node<Adder>("Adder");
    auto comparator = library.register_node<Comparator>("Comparator");

    pipeline::PipelineDescription desc(alloc, library);
  
    auto add_node = desc.add("Adder");
    auto compare_node = desc.add("Comparator");

    desc.connect(add_node, compare_node);

    desc.add_output(compare_node);

    WHEN("Pipeline is instantiated")
    {
      pipeline::Pipeline p(desc, 1, 1, alloc);

    }
  }
}