#pragma once

#include <algorithm>
#include <memory>
#include <functional>
#include <string>
#include <tuple>
#include <typeindex>
#include <unordered_map>
#include <vector>

namespace pipeline {

namespace detail
{

template<std::size_t...> struct Seq {};
template<int N, std::size_t... S> struct GenS : GenS<N - 1, N - 1, S...> {};

template<std::size_t... S> struct GenS<0, S...> { typedef Seq<S...> type; };


template <typename Tuple, std::size_t I> struct TupleVisitor
{
  template <typename Cb, typename... Args> static void visit(Cb &cb)
  {
    TupleVisitor<Tuple, I - 1>::visit(cb);
    using ElementType = std::tuple_element<I - 1, Tuple>;
    cb.template call<
      I - 1,
      typename ElementType::type
      >();
  }
};

template <typename Tuple> struct TupleVisitor<Tuple, 0>
{
  template <typename Cb> static void visit(Cb &cb)
  {
  }
};

template <typename Func> class ComputeHelper
{
};

template <typename _ClassType, typename _ReturnType, typename... _ArgumentTypes>
  class ComputeHelper<_ReturnType(_ClassType::*)(_ArgumentTypes...) const>
{
public:
  using ClassType = _ClassType;
  using ReturnType = _ReturnType;
  using ArgumentTypes = std::tuple<_ArgumentTypes...>;
  using MethodType = ReturnType(ClassType::*)(_ArgumentTypes...) const;

  template <std::size_t I> struct ArgumentType
  {
    using base_type = typename std::tuple_element<I, ArgumentTypes>::type;
    using type = typename std::decay<base_type>::type;
  };

  template <typename Cb> void each_argument(Cb &cb)
  {
    TupleVisitor<ArgumentTypes, std::tuple_size<ArgumentTypes>::value>::visit(cb);
  }

  template <typename... Args> static void dispatch(ClassType &cls, MethodType fn, void **outputs, const void **extra_inputs, Args&&... args)
  {
    dispatch_impl(cls, fn, outputs, extra_inputs, typename GenS<sizeof...(_ArgumentTypes) - sizeof...(Args)>::type(), std::forward<Args&&>(args)...);
  }

private:
  template <std::size_t I> static const typename ArgumentType<I>::type *extract_argument(const void **args)
  {
    const void *arg = args[I];
    return reinterpret_cast<const typename ArgumentType<I>::type *>(arg);
  }

  template <typename... Args, std::size_t... S> static void dispatch_impl(ClassType &cls, MethodType fn, void **outputs, const void **extra_inputs, Seq<S...>, Args&&... args)
  {
    (cls.*fn)(std::forward<Args&&>(args)..., (*extract_argument<S>(extra_inputs))...);
  }
};

}

class Allocator
{
public:
  Allocator(const std::allocator<void> &std)
  : _std(std)
  {
  }

  template <typename T> using Std = std::allocator<T>;
  template <typename T> Std<T> std() const
  {
    return typename Std<void>::rebind<T>::other(_std);
  }

private:
  std::allocator<void> _std;
};

class Node
{
public:
  struct InstanceData
  {

  };

  struct RunnerData
  {

  };
};

namespace detail
{
using ComputeFunction = std::function<void(Node &,
                                           Node::InstanceData *,
                                           Node::RunnerData *,
                                           const void **,
                                           void **)>;

template <typename Helper, bool HasInstanceData, bool hasRunnerData> struct ComputeBuilder
{
  static ComputeFunction build()
  {
    return [](Node &n,
                     Node::InstanceData *inst,
                     Node::RunnerData *runner,
                     const void **extra_arguments,
                     void **output)
    {
    Helper::dispatch(
      reinterpret_cast<typename Helper::ClassType&>(n),
      &Helper::ClassType::compute,
      output,
      extra_arguments,
      *(typename Helper::ClassType::InstanceData *)inst,
      *(typename Helper::ClassType::RunnerData *)runner);
    };

  }
};

template <typename Helper> struct ComputeBuilder<Helper, true, false>
{
  static ComputeFunction build()
  {
    return [](Node &n,
                     Node::InstanceData *inst,
                     Node::RunnerData *runner,
                     const void **extra_arguments,
                     void **output)
    {
    Helper::dispatch(
      reinterpret_cast<typename Helper::ClassType&>(n),
      &Helper::ClassType::compute,
      output,
      extra_arguments,
      *(typename Helper::ClassType::InstanceData *)inst);
    };

  }
};

template <typename Helper> struct ComputeBuilder<Helper, false, true>
{
  static ComputeFunction build()
  {
    return [](Node &n,
                     Node::InstanceData *inst,
                     Node::RunnerData *runner,
                     const void **extra_arguments,
                     void **output)
    {
    Helper::dispatch(
      reinterpret_cast<typename Helper::ClassType&>(n),
      &Helper::ClassType::compute,
      output,
      extra_arguments,
      *(typename Helper::ClassType::RunnerData *)runner);
    };

  }
};

template <typename Helper> struct ComputeBuilder<Helper, false, false>
{
  static ComputeFunction build()
  {
    return [](Node &n,
                     Node::InstanceData *inst,
                     Node::RunnerData *runner,
                     const void **extra_arguments,
                     void **output)
    {
    Helper::dispatch(
      reinterpret_cast<typename Helper::ClassType&>(n),
      &Helper::ClassType::compute,
      output,
      extra_arguments);
    };

  }
};
}

class NodeType
{
public:
  class ConnectionDataType
  {
  public:
    template <typename T>static ConnectionDataType create()
    {
      ConnectionDataType inp(std::type_index(typeid(typename std::decay<T>::type)));
      return inp;
    }

    bool operator==(const ConnectionDataType &oth) const
    {
      return _type == oth._type;
    }

  private:
    ConnectionDataType(std::type_index type)
    : _type(type)
    {
    }

    std::type_index _type;
  };

  template <typename T> static NodeType create(Allocator alloc, const char *name)
  {
    static_assert(std::is_base_of<Node, T>::value, "T must derive pipeline::Node");

    NodeType type{
      alloc,
      name
    };

    using Helper = detail::ComputeHelper<decltype(&T::compute)>;
    Helper arg_helper;
    ArgumentStripper input_stripper{ type._inputs };
    arg_helper.each_argument(input_stripper);

    type._outputs.push_back(ConnectionDataType::create<typename Helper::ReturnType>());

    using InstanceUndefined = std::is_same<Node::InstanceData, typename T::InstanceData>;
    using RunnerUndefined = std::is_same<Node::RunnerData, typename T::RunnerData>;

    type._has_instance_data = !InstanceUndefined::value;
    type._has_runner_data = !RunnerUndefined::value;

  type._compute = detail::ComputeBuilder<
    Helper,
    !InstanceUndefined::value,
    !RunnerUndefined::value
    >::build();

    return type;
  }

  const std::string &name() const { return _name; }
  bool has_instance_data() const { return _has_instance_data; }
  bool has_runner_data() const { return _has_runner_data; }

  std::vector<ConnectionDataType> compute_inputs() const { return _inputs; }
  std::vector<ConnectionDataType> compute_outputs() const { return _outputs; }

  void compute(
    Node &t,
    Node::InstanceData *inst,
    Node::RunnerData *run,
    const void **args,
    void **out)
  {
    _compute(t, inst, run, args, out);
  }

private:
  NodeType(
    Allocator alloc,
    const char *name)
  : _name(name, alloc.std<char>())
  , _inputs(alloc.std<ConnectionDataType>())
  {
  }

  struct ArgumentStripper
  {
    template <std::size_t I, typename T> void call()
    {
      inputs.push_back(ConnectionDataType::create<T>());
    }

    std::vector<ConnectionDataType> &inputs;
  };

  std::string _name;
  detail::ComputeFunction _compute;
  std::vector<ConnectionDataType> _inputs;
  std::vector<ConnectionDataType> _outputs;
  bool _has_instance_data;
  bool _has_runner_data;
};

class NodeLibrary
{
public:
  NodeLibrary(Allocator &alloc)
  : _allocator(alloc)
  , _nodes(_allocator.std<NodeType>())
  {
  }

  template <typename T> std::shared_ptr<const NodeType> register_node(const char *name)
  {
    auto ptr = std::allocate_shared<NodeType>(_allocator.std<NodeType>(), NodeType::create<T>(_allocator, name));
    _nodes[std::string(name, _allocator.std<char>())] = ptr;
    return ptr;
  }

  std::size_t node_count() const
  {
    return _nodes.size();
  }

  std::shared_ptr<const NodeType> find(const char *name) const
  {
    auto it = _nodes.find(std::string(name, _allocator.std<char>()));
    if (it == _nodes.end())
    {
      return nullptr;
    }
    return it->second;
  }

  bool can_connect(const std::shared_ptr<const NodeType> &source, const std::shared_ptr<const NodeType> &dest) const
  {
    auto output = source->compute_outputs();
    auto input = dest->compute_inputs();

    if (input.size() > output.size())
    {
      return false;
    }

    for (std::size_t i = 0; i < input.size(); ++i)
    {
      if (!can_connect(output[i], input[i]))
      {
        return false;
      }
    }

    return true;
  }

  bool can_connect(const NodeType::ConnectionDataType &source, const NodeType::ConnectionDataType &dest) const
  {
    return source == dest;
  }

private:
  Allocator _allocator;
  using PairType = std::pair<const std::string, std::shared_ptr<const NodeType>>;
  std::unordered_map<
    std::string,
    std::shared_ptr<const NodeType>,
    std::hash<std::string>,
    std::equal_to<std::string>,
    Allocator::Std<PairType>> _nodes;
};

class PipelineDescription
{
public:
  class NodeId
  {
  public:
    std::size_t identifier;

    bool operator==(const NodeId &oth) const
    {
      return identifier == oth.identifier;
    }
  };

  class ConnectionId
  {
  public:
    std::size_t identifier;

    bool operator==(const ConnectionId &oth) const
    {
      return identifier == oth.identifier;
    }
  };

  PipelineDescription(Allocator &allocator, NodeLibrary &library)
  : _allocator(allocator)
  , _library(library)
  , _nodes(_allocator.std<InstanceDescription>())
  , _connections(_allocator.std<std::pair<NodeId, NodeId>>())
  , _required_nodes(_allocator.std<InstanceDescription>())
  {
  }

  std::size_t node_count() const { return _nodes.size(); }

  NodeId add(const char *name)
  {
    auto type = _library.find(name);
    if (!type)
    {
      throw std::logic_error("Invalid type specified");
    }

    NodeId id{ _nodes.size() };
    InstanceDescription desc{ id, type };
    _nodes.push_back(desc);
    return id;
  }

  bool can_connect(NodeId source, NodeId dest) const
  {
    if (are_connected(source, dest))
    {
      return false;
    }

    auto &src_node = _nodes[source.identifier];
    auto &dest_node = _nodes[dest.identifier];

    return _library.can_connect(src_node.type, dest_node.type);
  }

  bool are_connected(NodeId source, NodeId dest) const
  {
    return std::find(_connections.begin(), _connections.end(), std::make_pair(source, dest)) != _connections.end();
  }

  ConnectionId connect(NodeId source, NodeId dest)
  {
    if (!can_connect(source, dest))
    {
      throw std::runtime_error("Invalid connection");
    }

    _connections.emplace_back(source, dest);
    return ConnectionId{ _connections.size() };
  }

  void add_output(NodeId out)
  {
    _required_nodes.push_back(out);
  }

private:
  struct InstanceDescription
  {
    NodeId id;
    std::shared_ptr<const NodeType> type;
  };
  Allocator _allocator;
  const NodeLibrary &_library;
  std::vector<InstanceDescription> _nodes;
  std::vector<std::pair<NodeId, NodeId>> _connections;
  std::vector<NodeId> _required_nodes;
};

class Pipeline
{
public:
  class Worker
  {
  };

  class Instance
  {

  };

  Pipeline(const PipelineDescription &description, std::size_t instance_count, std::size_t runner_count, Allocator &allocator)
  : _allocator(allocator)
  , _description(description)
  , _workers(_allocator.std<Worker>())
  , _instances(_allocator.std<Instance>())
  {
    _workers.resize(runner_count);
    _instances.resize(instance_count);
  }

private:
  Allocator &_allocator;
  const PipelineDescription &_description;
  std::vector<Worker> _workers;
  std::vector<Instance> _instances;
};

}
